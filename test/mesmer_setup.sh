#!/bin/sh

wkdir=`pwd`
gendir=${wkdir}/../mesmer

if [ ! -d ${gendir} ] 
then
    mkdir ${gendir}
    cd ${gendir}
    echo "Installing MESMER at directory: " ${gendir} 
    git clone https://gitlab.cern.ch/muesli/nlo-mc/mesmer-dev.git -b v1.2.0-dev .
#    make -j14
    make
else
    cd ${gendir}
    echo "MESMER installation directory is: " ${gendir}
    make
fi

cd ${wkdir}
