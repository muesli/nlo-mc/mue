#!/bin/sh

writer=`pwd`
gen=${writer}/../mesmer
src=${writer}/../code
test=${writer}/../test

# MESMER setup
source ${test}/mesmer_setup.sh

MAIN=mesmer_writer

echo "ROOTSYS = " ${ROOTSYS}
ROOTINCDIR=`$ROOTSYS/bin/root-config --incdir`
ROOTLIBS=`$ROOTSYS/bin/root-config --cflags --libs`
echo "ROOTINCDIR = " ${ROOTINCDIR}
echo "ROOTLIBS = " ${ROOTLIBS}

# working directory
JOBDIR=test_mesmer_writer_`date +"%y-%m-%d_%T"`
echo "Working directory is: " ${JOBDIR}
mkdir ${JOBDIR}
cd ${JOBDIR}

echo "Compiling ..."

rootcling -f MuEtreeWDict.C -I${src} MuEtree.h Utils.h -I${writer} MuEtreeWLinkDef.h

OPTIONS="-O2 -Wall"
#OPTIONS="-g -Wall"

g++ ${OPTIONS} -shared -fPIC -o libMuEtree.so -I${src} MuEtreeWDict.C ${src}/MuEtree.cc ${src}/Utils.cc ${ROOTLIBS}

g++ ${OPTIONS} -o ${MAIN}.exe -I${src} -I${ROOTINCDIR} ${writer}/${MAIN}.cc MuEtreeWDict.C ${src}/MuEtree.cc ${src}/Utils.cc ${ROOTLIBS} -L${gen}/ -lmesmerfull -lX11 -lgfortran -lquadmath 

#return 0

cat > mesmer.cards <<!
mode weighted
nev 100000
nwarmup 10000
seed 42
Qmu 1
Ebeam 160.
bspr  3.75
nphot 1020
Eemin 0.
thmumin 0.1
path test-run
run
!

echo "Running " ${MAIN}.exe
#time ./${MAIN}.exe  mesmer.cards  mesmer_events.root
time ./${MAIN}.exe  mesmer.cards  mesmer_events.root  >&  ${MAIN}.log 2>&1

rm *.exe *.C

echo "Done."
